import React, { useState, useEffect } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Product from './Components/Product';
import Customer from './Components/Customer';
import OrderAll from './Components/OrderAll';
import Nav_btn from './Components/Nav_btn';
import Login from './Components/Login';

function App() {
  const [_modalloginclose, set_modalloginclose] = useState(false);

  useEffect(() => {
    if (sessionStorage.getItem('IdAdmin')) {
      set_modalloginclose(false)
    } else {
      set_modalloginclose(true)
    }

  }, [])

  return (
    <div className="App">
      <Router>

        <Nav_btn
          onOpen={() => {
            set_modalloginclose(true)
          }}
        />

        <Switch>
          <Route path="/Customer">
            <Customer

            />
          </Route>
          <Route path="/Product">
            <Product
            />
          </Route>
          <Route path="/Order">
            <OrderAll
            />
          </Route>
          <Route path="/">
            <Customer />
          </Route>
        </Switch>

      </Router>
      <Login
        onclose={() => {
          set_modalloginclose(false)
        }}
        closelogin={_modalloginclose}
      />
    </div >
  );
}

export default App;