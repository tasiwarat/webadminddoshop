import axios from 'axios';

function PostProduct(name) {

    return new Promise((resolve, reject) => {
        axios.post('http://localhost:5000/product', {
            name: name
        }).then(res => {
            console.log(res);
            resolve(res)
        }).catch(err => {
            console.log(err.response.data.message);
            reject(err)
        })
    })
}
export default PostProduct;