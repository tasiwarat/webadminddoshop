import axios from 'axios';

function GetLogin(user, password) {
    return new Promise ((resolve, reject) =>{
        axios.get('http://localhost:5000/login?user='+user+'&pass='+password).then(res => {
            console.log(res);
            resolve(res);
        }).catch(err => {
            console.log(err.response.data.message);
            reject(err);
        })
    }
    )
}
export default GetLogin;