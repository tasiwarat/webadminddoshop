import axios from 'axios'


function PostCustomer (user) {
    return new Promise ((resolve, reject) =>{
        axios.get('http://localhost:5000/getuser', {
            user: user
        }).then(res => {
            console.log(res);
            resolve(res);
        }).catch(err => {
            console.log(err.response.data.message);
            reject(err);
        })

    })

}

export default PostCustomer;