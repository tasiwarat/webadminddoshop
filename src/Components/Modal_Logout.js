import React from 'react';
import Modal from '@material-ui/core/Modal';
import './Modal_Logout.css';

function Modal_Logout(props) {
    return (
        <Modal
            open={props.open}
        >
            <div className="Modal_Logout_Fullpage">
                <div className="Modal_Logout_Show">
                    <div className="Modal_Logout_sect2">
                        Are you sure?
                    </div>
                    <div className="Modal_Logout_sect3">
                        <button
                            className="Modal_Logout_sect3_btnYes"
                            onClick={() => {
                                props.onclose();
                                sessionStorage.removeItem('IdAdmin');
                                window.location.href = './Customer';
                            }}
                        >
                            Yes
                        </button>
                        <button
                            className="Modal_Logout_sect3_btnNo"
                            onClick={() => {
                                props.onclose()
                            }}
                        >
                            No
                        </button>
                    </div>
                </div>

            </div>
        </Modal>
    )
}
export default Modal_Logout;