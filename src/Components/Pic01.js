import React from 'react';
import './Pic01.css';
import '../Global.css';

function Pic01(props) {

    return (
        <div className="pic">
            <div className="Pic_Div">
                <div className="Pic_Div_01">
                    <img src={props.dataItem.image}></img>
                </div>
                <div className="pic_slide">
                    <div className="pic_slide_text1">
                    <div className="Body_text">Name: {props.dataItem.name}</div>
                    </div>
                    <div className="pic_slide_text2">
                    <div className="Body_text">Price: {props.dataItem.price}</div>
                    </div>
                </div>
            </div>
        </div>

    );

}

export default Pic01;