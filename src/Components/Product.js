import React, { useEffect, useState } from 'react';
import Postproduct from '../Api/Postproduct';
import './Product.css';
import Moment from 'react-moment';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import ModalProduct from './ModalProduct';

function Product() {

    const [_getproductdetail, set_getproductdetail] = useState([]);
    const [_searchtext, set_searchtext] = useState("");
    const [_datasearch, set_datasearch] = useState([]);
    const [_btnProductDetail, set_btnProductDetail] = useState(false);
    const [_testDetailPro, set_testDetailPro] = useState([]);

    useEffect(() => {
        Postproduct("").then((res) => {
            console.log(res.data.data)
            set_getproductdetail(res.data.data)
            set_datasearch(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    function get_searchtext(val) {
        set_searchtext(val.target.value)
    }
    function searchtext() {
        if (_getproductdetail.length > 0) {
            if (_searchtext == "") {
                set_datasearch(_getproductdetail)
            } else {
                set_datasearch(_getproductdetail.filter(res => (res.name).toLowerCase().includes(_searchtext)))
            }
        } else {
            alert("can't search")
        }
    }
    return (
        <div className="Product_FullPage">
            <div className="Product_Search">
                <div className="Product_Search_Fixed">
                    <div className="Product_Search1">
                        <div className="Head_text">
                            <div className="Head_Page">Product</div>
                        </div>
                        <Input
                            placeholder="Search Name..."
                            id="input-with-icon-adornment"
                            value={_searchtext}
                            onChange={get_searchtext}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => { searchtext() }}
                                        type="submit"
                                        aria-label="search">
                                        <SearchIcon />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </div>
                </div>
            </div>
            <div className="Product_Show">
                <div className="Product_show_all">
                    <div className="Product_Head">
                        <div className="Product_Head1">
                            <div className="Body_text_head">index</div>
                        </div>
                        <div className="Product_Head3">
                            <div className="Body_text_head">name</div>
                        </div>
                        <div className="Product_Head5">
                            <div className="Body_text_head">price</div>
                        </div>
                        <div className="Product_Head6">
                            <div className="Body_text_head">option</div>
                        </div>
                        <div className="Product_Head7">
                            <div className="Body_text_head">quality</div>
                        </div>
                        <div className="Product_Head8">
                            <div className="Body_text_head">UpdateAt</div>
                        </div>
                        <div className="Product_Head9">
                            <div className="Body_text_head">Detail</div>
                        </div>
                    </div>
                    {_datasearch.map(res => {
                        return (
                            <div className="Product_Body">
                                <div className="Product_Body1">
                                    <div className="Body_text">{res.index}</div>
                                </div>
                                <div className="Product_Body3">
                                    <div className="text_overflow Body_text">{res.name}</div>
                                </div>
                                <div className="Product_Body5">
                                    <div className="Body_text">{res.price}</div>
                                </div>
                                <div className="Product_Body6">
                                    <div className="Body_text">{res.option}</div>
                                </div>
                                <div className="Product_Body7">
                                    <div className="Body_text">{res.quality}</div>
                                </div>
                                <div className="Product_Body8">
                                    <div className="Body_text">
                                        <Moment date={new Date(res.UpdateAt)} format="YYYY MMM DD HH:mm" />
                                    </div>
                                </div>
                                <div className="Product_Body9">
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => {
                                            set_btnProductDetail(true)
                                            set_testDetailPro(res)
                                        }}
                                    >
                                        Detail
                                    </Button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <ModalProduct
                open={_btnProductDetail}
                onclose={() => { set_btnProductDetail(false) }}
                history={_testDetailPro}
            />
        </div>
    )
}
export default Product;