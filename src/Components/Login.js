import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import './Login.css';
import '../Global.css';
import Modal from '@material-ui/core/Modal';
import GetLogin from '../Api/GetLogin';

function Login(props) {
    const [_textid, set_textid] = useState("");
    const [_textpw, set_textpw] = useState("");

    function gettextid(val) {
        set_textid(val.target.value)
    }
    function gettextpw(val) {
        set_textpw(val.target.value)
    }
    function clicksubmit() {
        if (_textid == "Admin") {
            GetLogin(_textid, _textpw).then((res) => {
                console.log(res)
                sessionStorage.setItem('IdAdmin', JSON.stringify(res.data.data))
                props.onclose()
            }).catch((err) => {
                console.log(err)
                alert("check your password")
            })
        } else {
            alert("try again")
        }
    }

    return (
        <Modal
            open={props.closelogin}
        >
            <div className="Login_FullPage">
                <div className="Login_Body">
                    <div className="Login_Body_Head">
                        <div className="Header_text_L">
                            D-DO SHOP
                        </div>
                    </div>
                    <div className="Login_Body_ID">
                        <div className="Login_Body_ID_head">
                            <div className="Body_text_head">ID:</div>
                        </div>
                        <TextField
                            className="Login_Body_ID_text"
                            label="User ID"
                            id="outlined-start-adornment"
                            variant="outlined"
                            value={_textid}
                            onChange={gettextid}
                        />
                    </div>
                    <div className="Login_Body_Password">
                        <div className="Login_Body_Password_head">
                            <div className="Body_text_head"> Password:</div>
                        </div>
                        <TextField
                            className="Login_Body_Password_text"
                            label="Password"
                            id="outlined-start-adornment"
                            variant="outlined"
                            type="password"
                            value={_textpw}
                            onChange={gettextpw}
                        />
                    </div>
                    <div className="Login_Body_Submit">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                clicksubmit()
                            }}
                        >
                            Login
                        </Button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}
export default Login;