import React, { useState, useEffect } from 'react';
import './Nav_btn.css';
import { Link } from "react-router-dom";
import '../Global.css';
import { Button } from '@material-ui/core';
import { useLocation } from 'react-router-dom';
import Modal_Logout from './Modal_Logout';


function Nav_btn(props) {
    const [_indexpage, set_indexpage] = useState(0)
    const [_Modal_Logout, set_Modal_Logout] = useState(false)
    const [_chagenclassname, set_changeclassname] = useState([])
    const objMenu = [
        {
            index: 0,
            className: "Nav_Link",
            title: "Product",
            active: 1
        },
        {
            index: 1,
            className: "Nav_Link",
            title: "Customer",
            active: 0
        },
        {
            index: 2,
            className: "Nav_Link",
            title: "Order",
            active: 0
        }
    ]

    useEffect(() => {

        const pathname = window.location.pathname
        const GenMenu = [];
        objMenu.forEach(res => {
            var _active = 0;
            if (pathname == "/" + res.title) {
                _active = 1;
            }
            GenMenu.push({
                index: res.index,
                className: res.className,
                title: res.title,
                active: _active
            })
        })
        set_changeclassname(GenMenu)
        console.log(pathname);
    }, [])

    // const usePathname = () => {
    //     const location = useLocation();
    //     return location.pathname;
    //   }

    return (
        <div className="Nav_left">
            <div className="MemuBar">
                <div className="LogoBar">
                    <div className="Header_text_M">D-DO</div>
                    <div className="Header_text_L">S  H  O  P</div>
                </div>
                <div className="RowBar">
                    {_chagenclassname.map(res => {
                        return (
                            <div className="Rowbar_div">
                                <div className="RowBar_text">
                                    <div to={"/" + res.title}
                                        onClick={() => {
                                            window.location = "/" + res.title;
                                        }}
                                        className={res.active == 1 ? "Nav_Linkactive" : "Nav_Link"}
                                    >
                                        {res.title}
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                    <div className="Rowbar_div">
                        <div className="RowBar_text">
                            <div className="Nav_Link"
                                onClick={() => {
                                    set_Modal_Logout(true);
                                }}
                            >
                                Log Out
                            </div>
                        </div>
                    </div>
                    <Modal_Logout
                        open={_Modal_Logout}
                        onclose={() => {
                            set_Modal_Logout(false)
                        }}
                    />
                </div>
            </div>

        </div>
    )
}
export default Nav_btn;