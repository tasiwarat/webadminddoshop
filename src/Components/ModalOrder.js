import React from 'react';
import './ModalOrder.css';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

function ModalOrder(props) {

    return (
        <Modal
            open={props.open}
        >
            <div className="ModalOrder_FullPage">
                <div className="ModalOrder_show">
                    <div className="ModalOrder_Head">
                        <div className="ModalOrder_Head1">
                            <div className="Body_text_head"></div>
                        </div>
                        <div className="ModalOrder_Head2">
                            <div className="Body_text_head">Name</div>
                        </div>
                        <div className="ModalOrder_Head3">
                            <div className="Body_text_head">Description</div>
                        </div>
                        <div className="ModalOrder_Head4">
                            <div className="Body_text_head">Price</div>
                        </div>
                        <div className="ModalOrder_Head5">
                            <div className="Body_text_head">Quality</div>
                        </div>
                        <div className="ModalOrder_Head6">
                            <div className="Body_text_head">Summary</div>
                        </div>
                    </div>
                    {props.history.map(res => {
                        return (
                            <div className="ModalOrder_Body">
                                <div className="ModalOrder_Body1">
                                    <img src={res.image}></img>
                                </div>
                                <div className="ModalOrder_Body2">
                                    <div className="text_wrap2">{res.name}</div>
                                </div>
                                <div className="ModalOrder_Body3">
                                    <div className="text_wrap3">{res.description}</div>
                                </div>
                                <div className="ModalOrder_Body4">
                                    {res.price}
                                </div>
                                <div className="ModalOrder_Body5">
                                    {res.quality}
                                </div>
                                <div className="ModalOrder_Body6">
                                    {res.summary}
                                </div>
                            </div>
                        )
                    })}
                    <div className="Modal_btnClose">
                        <Button
                            variant="contained"
                            color="secondary"
                            fullWidth={true}
                            onClick={() => {
                                props.onmodalclose()
                            }}
                        >
                            close
                        </Button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}
export default ModalOrder;