import React, { useState, useEffect } from 'react';
import PostCustomer from '../Api/PostCustomer';
import './Customer.css';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Moment from 'react-moment';

function Customer() {
    const [_userCustomer, set_userCustomer] = useState([]);
    const [_searchtextCustomer, set_searchtextCustomer] = useState("");
    const [_datasearchCustomer, set_datasearchCustomer] = useState([]);

    useEffect(() => {
        PostCustomer("").then((res) => {
            console.log(res.data.data)
            set_userCustomer(res.data.data)
            set_datasearchCustomer(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [])
    function get_searchtext(val) {
        set_searchtextCustomer(val.target.value)
    }
    function searchtext() {
        if (_userCustomer.length > 0) {
            if (_searchtextCustomer == "") {
                set_datasearchCustomer(_userCustomer)
            } else {
                set_datasearchCustomer(_userCustomer.filter(res => (res.Id).toLowerCase().includes(_searchtextCustomer)))
            }
        } else {
            alert("can't search")
        }
    }
    return (

        <div className="Customer_Fullpage">
            <div className="Customer_Search">
                <div className="Customer_Saerch_fixed">
                    <div className="Customer_Search1">
                        <div className="Head_text">
                            <div className="Head_Page">Customer</div>
                        </div>
                        <Input
                            placeholder="Search Name..."
                            id="input-with-icon-adornment"
                            value={_searchtextCustomer}
                            onChange={get_searchtext}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => { searchtext() }}
                                        type="submit"
                                        aria-label="search">
                                        <SearchIcon />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </div>
                </div>
            </div>
            <div className="Customer_Sect2">
                <div className="Customer_Sect2_1">
                    <div className="Customer_HeadTable">
                        <div className="Customer_HeadTable1">
                            <div className="Body_text_head"> ID </div>
                        </div>
                        <div className="Customer_HeadTable3">
                            <div className="Body_text_head">Email </div>
                        </div>
                        <div className="Customer_HeadTable4">
                            <div className="Body_text_head"> Status </div>
                        </div>
                        <div className="Customer_HeadTable5">
                            <div className="Body_text_head"> UpdateAt </div>
                        </div>
                    </div>
                    {_datasearchCustomer.map(res =>
                    (
                        <div className="Customer_Body">
                            <div className="Customer_Body1">
                                <div className="Body_text">{res.Id}</div>
                            </div>
                            <div className="Customer_Body3">
                                <div className="Body_text">{res.Email}</div>
                            </div>
                            <div className="Customer_Body4">
                                <div className="Body_text">{res.Status}</div>
                            </div>
                            <div className="Customer_Body5">
                                <div className="Body_text">
                                    <Moment date={new Date(res.UpdateAt)} format="YYYY MMM DD HH:mm" />
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>

    )

}
export default Customer;