import React from 'react';
import './ModalProduct.css';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import Moment from 'react-moment';

function ModalProduct(props) {

    return (
        <Modal
            open={props.open}
        >
            <div className="ModalProduct_FullPage">
                <div className="ModalProduct_show">
                    <div className="ModalProduct_Head">
                        <div className="ModalProduct_Head1">
                            <div className="ModalProduct_Image">
                                <img src={props.history.image}></img>
                            </div>
                        </div>
                        <div className="ModalProduct_Head2">
                            <div className="ModalProduct_Head2_2">
                                <div >{props.history.name}</div>
                            </div>
                            <div className="ModalProduct_Head2_3">
                                <div >{props.history.description}</div>
                            </div> 
                            <div className="ModalProduct_Head2_1">
                                <div className="Body_text_head">ID:</div>
                                <div className="Body_text_pro4">{props.history.index}</div>
                            </div>
                            <div className="ModalProduct_Head2_7">
                                <div className="Body_text_head">Option:</div>
                                <div className="Body_text_pro4">{props.history.option}</div>
                            </div>    
                            <div className="ModalProduct_Head2_4">
                            <div className="Body_text_head">Price:</div>
                                <div className="Body_text_pro4">฿{props.history.price}</div>
                            </div>                    
                            <div className="ModalProduct_Head2_5">
                                <div className="Body_text_head">Quality:</div>
                                <div className="Body_text_pro4">{props.history.quality}</div>
                            </div>
                            <div className="ModalProduct_Head2_6">
                                <div className="Body_text_head">Update:</div>
                                <div className="Body_text_pro4">
                                <Moment date={new Date(props.history.UpdateAt)} format="YYYY MMM DD HH:mm" />
                                </div>
                            </div>
                            <div className="Modal_btnClose"> 
                            <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                                props.onclose()
                            }}
                        >
                            close
                        </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}
export default ModalProduct;