import React, { useEffect, useState } from 'react';
import PostOrder from '../Api/PostOrder';
import './OrderAll.css';
import GetRef from '../Api/GetRef';
import ModalOrder from './ModalOrder';
import SearchIcon from '@material-ui/icons/Search';
import Moment from 'react-moment';
import Button from '@material-ui/core/Button';
import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from 'styled-components';
import { colorBTN } from '../theme';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';

function OrderAll(props) {
    const [_showorder, set_showorder] = useState([]);
    const [_orderhistory, set_orderhistory] = useState([]);
    const [_closemodal, set_closemodal] = useState(false);
    const [_searchorderall, set_searchorderall] = useState([]);
    const [_getsearchtextorderall, set_getsearchtextorderall] = useState("");

    useEffect(() => {
        PostOrder("").then((res) => {
            console.log(res)
            set_showorder(res.data.data)
            set_searchorderall(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    function get_textorderall(val) {
        set_getsearchtextorderall(val.target.value)
    }

    function searchOrderall() {
        if (_showorder.length > 0) {
            if (_getsearchtextorderall == "") {
                set_searchorderall(_showorder)
            } else {
                set_searchorderall(_showorder.filter(res => (res.RefId).includes(_getsearchtextorderall)))
            }
        } else {

        }
    }

    return (
        <ThemeProvider theme={colorBTN}>
            <div className="OrderAll_FullPage">
                <div className="Orderall_Search">
                    <div className="Orderall_Search_Fixed">
                        <div className="Orderall_Search1">
                            <div className="Head_text">
                                <div className="Head_Page">Order</div>
                            </div>
                            <Input
                                placeholder="Search Name..."
                                id="input-with-icon-adornment"
                                value={_getsearchtextorderall}
                                onChange={get_textorderall}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            onClick={() => { searchOrderall() }}
                                            type="submit"
                                            aria-label="search">
                                            <SearchIcon />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </div>
                    </div>
                </div>
                <div className="OrderAll_sect2">
                    <div className="OrderAll_sect2_1">
                        <div className="OrderAll_Header">
                            <div className="OrderAll_Header3">
                                <div className="Body_text_head">RefId</div>
                            </div>
                            <div className="OrderAll_Header1">
                                <div className="Body_text_head">ID</div>
                            </div>
                            <div className="OrderAll_Header5">
                                <div className="Body_text_head">CreateDate</div>
                            </div>
                            <div className="OrderAll_Header2">
                                <div className="Body_text_head">Price</div>
                            </div>
                            <div className="OrderAll_Header6">
                                <div className="Body_text_head">Detail</div>
                            </div>
                        </div>
                        {_searchorderall.map(res => {
                            return (
                                <div className="OrderAll_Body">
                                    <div className="OrderAll_Body3">
                                        <div className="Body_text">{res.RefId}</div>
                                    </div>
                                    <div className="OrderAll_Body1">
                                        <div className="Body_text">{res.Id}</div>
                                    </div>
                                    <div className="OrderAll_Body5">
                                        <div className="Body_text">
                                            <Moment date={new Date(res.CreateDate)} format="DD MMM YYYY HH:mm" />
                                        </div>
                                    </div>
                                    <div className="OrderAll_Body2">
                                        <div className="Body_text">{res.Price}</div>
                                    </div>
                                    <div className="OrderAll_Body6">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={() => {
                                                GetRef(res.RefId).then((res) => {
                                                    console.log(res.data.data)
                                                    set_orderhistory(res.data.data)
                                                    set_closemodal(true)
                                                }).catch((err) => {
                                                    console.log(err)
                                                })
                                            }}
                                        >
                                            <div>Detail</div>
                                        </Button>
                                    </div>
                                </div>
                            )
                        })
                        }
                    </div>
                </div>
                <ModalOrder
                    history={_orderhistory}
                    open={_closemodal}
                    onmodalclose={() => { set_closemodal() }}
                />
            </div>
        </ThemeProvider>
    )
}
export default OrderAll;