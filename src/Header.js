import React, { useState, useEffect } from 'react';
import './Header.css';
import Nav_btn from './Components/Nav_btn';
import Login from './Components/Login';
import Button from '@material-ui/core/Button';
import OrderAll from './Components/OrderAll';

function Header() {
    const [_modalloginclose, set_modalloginclose] = useState(false);

    useEffect(() => {
        if (sessionStorage.getItem('IdAdmin')) {
            set_modalloginclose(false)
        } else {
            set_modalloginclose(true)
        }
    }, [])

    return (
        <div className="Header_Nav_Top">
            <div className="Header_Header">
                <div className="Header_Header_sect1">
                </div>
                <div className="Header_Header_sect2">
                    <input
                        className="Orderall_textfield"
                        value={_getsearchtext}
                        onChange={get_text}
                    />
                    <button
                        onClick={() => {
                            (<OrderAll
                                text_order={_getsearchtext}
                            />)
                        }}
                    >
                        Search
                    </button>
                </div>
                <div className="Header_Header_sect3">
                    <div className="Header_btn">
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                                sessionStorage.removeItem('IdAdmin')
                                set_modalloginclose(true)
                            }}
                        >
                            Log Out
                        </Button>
                    </div>
                </div>
            </div>
            <Nav_btn />

            <Login
                closelogin={_modalloginclose}
                onclose={() =>
                    set_modalloginclose(false)
                }
            />
        </div >
    )
}
export default Header;